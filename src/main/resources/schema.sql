-- 初始化

-- 建文件表
DROP TABLE IF EXISTS File_Tables;
CREATE TABLE File_Tables
(
    ID                BIGINT        NOT NULL COMMENT '主键ID',
    name              VARCHAR2(50)  NOT NULL COMMENT '名称',
    path              VARCHAR2(100) NOT NULL COMMENT '存放位置',
    file_size         BIGINT        NOT NULL COMMENT '文件大小',
    another_name      VARCHAR2(50)  NOT NULL COMMENT '别名',
    PRIMARY KEY (ID)
);
COMMENT ON TABLE File_Tables IS '文件表';

-- 建数据表
DROP TABLE IF EXISTS Data_Tables;
CREATE TABLE Data_Tables
(
    ID                BIGINT        NOT NULL COMMENT '主键ID',
    File_Table_id     BIGINT        NOT NULL COMMENT '文件表id',
    File_Name         VARCHAR2(50)  NOT NULL COMMENT '文件表名',
    name              VARCHAR2(100) NOT NULL COMMENT '名称',
    start_time        BIGINT        NOT NULL COMMENT '开始时间',
    end_time          BIGINT        NOT NULL COMMENT '结束时间',
    PRIMARY KEY (ID)
);
COMMENT ON TABLE Data_Tables IS '数据表';
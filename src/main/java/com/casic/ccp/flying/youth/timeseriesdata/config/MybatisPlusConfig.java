package com.casic.ccp.flying.youth.timeseriesdata.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * mybatis_plus 的配置类(配置了分页插件)
 * @author: 飞航青年队
 */
@EnableTransactionManagement
@Configuration
@MapperScan("com.casic.ccp.flying.youth.timeseriesdata.dao")
public class MybatisPlusConfig {

    /**
     * mybatis 拦截器
     * 最新版
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.H2));
        return interceptor;
    }

}

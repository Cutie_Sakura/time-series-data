package com.casic.ccp.flying.youth.timeseriesdata.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
/**
 * @author: 飞航青年队
 */
@TableName(value = "file_tables")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadFile {
    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 文件名称
     */
    private String name;
    /**
     * 文件路径
     */
    private String path;
    /**
     * 文件大小
     */
    @TableField(value = "file_size")
    private Long fileSize;
    /**
     * 别名
     */
    @TableField(value = "another_name")
    private String anotherName;

    public UploadFile() {
    }

    public UploadFile(Long id, String name, String path, String anotherName) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.anotherName = anotherName;
    }

    @Override
    public String toString() {
        return "UploadFile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", anotherName='" + anotherName + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAnotherName() {
        return anotherName;
    }

    public void setAnotherName(String anotherName) {
        this.anotherName = anotherName;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }
}

package com.casic.ccp.flying.youth.timeseriesdata.result;
/**
 * @author: 飞航青年队
 */
public class ResultData {
    private Long total;
    private Object items;

    public ResultData() {
    }

    public ResultData(Object items) {
        this.items = items;
    }

    public ResultData(Long total, Object items) {
        this.total = total;
        this.items = items;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Object getItems() {
        return items;
    }

    public void setItems(Object items) {
        this.items = items;
    }
}

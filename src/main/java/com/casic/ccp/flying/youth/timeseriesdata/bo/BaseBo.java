package com.casic.ccp.flying.youth.timeseriesdata.bo;

/**
 * @author: tian_gui_yin
 */
public class BaseBo {
    /**
     *  默认查询数量
     */
    private Integer limit = 20;
    /**
     * 默认页码
     */
    private Integer page = 1;
    /**
     * 排序规则
     */
    private String sort = "asc";

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "BaseBo{" +
                "limit=" + limit +
                ", page=" + page +
                ", orderBy='" + sort + '\'' +
                '}';
    }
}

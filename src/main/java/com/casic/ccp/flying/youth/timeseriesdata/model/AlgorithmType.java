package com.casic.ccp.flying.youth.timeseriesdata.model;

/**
 * 算法类型
 *
 * @Author: 贺坤
 * @Date: 2021/9/14 9:44
 */
public enum AlgorithmType {
    /**
     * 不使用算法
     */
    Null,
    /**
     * 最大值
     */
    MaximumValue,
    /**
     * 最小值
     */
    MinimumValue,
    /**
     * 平均值
     */
    AverageValue;
}

package com.casic.ccp.flying.youth.timeseriesdata.model;


import java.util.Date;


/**
 * 数据
 *
 * @Author: 贺坤
 * @Date: 2021/1/27 21:38
 */
public class Data {
    /**
     * 时间戳
     */
    private Date ts;
    /**
     * 值
     */
    private Float value;

    public Data() {

    }

    public Data(Date ts, Float value) {
        this.ts = ts;
        this.value = value;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return ts +":" + value;
    }
}

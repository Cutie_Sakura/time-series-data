package com.casic.ccp.flying.youth.timeseriesdata.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.casic.ccp.flying.youth.timeseriesdata.bo.DataLoadInfoBo;
import com.casic.ccp.flying.youth.timeseriesdata.model.DataLoadInfo;
import com.casic.ccp.flying.youth.timeseriesdata.result.GlobalResult;
import com.casic.ccp.flying.youth.timeseriesdata.result.ResultData;
import com.casic.ccp.flying.youth.timeseriesdata.result.ResultMessage;
import com.casic.ccp.flying.youth.timeseriesdata.service.DataLoadService;
import com.casic.ccp.flying.youth.timeseriesdata.service.TaosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 数据管理接口
 * @Author: 贺坤
 * @Date: 2021/9/8 16:27
 */
@RequestMapping("/data")
@Controller
public class DataController {

    @Autowired
    private DataLoadService dataLoadService;
    /**
     * 涛思数据 服务
     */
    @Autowired
    TaosService taosService;
    /**
     * 数据列表查询接口
     */
    @GetMapping("/getDataList")
    @ResponseBody
    public GlobalResult getDataLoadInfoList(DataLoadInfoBo dataLoadInfoBo){
        IPage<DataLoadInfo> pages = dataLoadService.getList(dataLoadInfoBo);
        return new GlobalResult(new ResultData(pages.getTotal(),pages.getRecords()));
//        return new GlobalResult(new ResultData(pages.getTotal(),pages.getRecords()));
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ResponseBody
    public ResultMessage deleteById(Long  id){
        DataLoadInfo dataLoadInfo = dataLoadService.getById(id);
        taosService.delTableByName(dataLoadInfo.getName());
        dataLoadService.delete(id);
        return new ResultMessage("删除","删除成功！","success");
    }


}

/**
 * Copyright (C), 2021-2021, Sakura有限公司(尚未成立)
 * FileName: TaosDao
 * Author:   Sakura
 * Date:     2021/9/6 20:25
 * Description: 涛思数据库相关操作,显示库信息,创建表等
 * History: 修改历史
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.casic.ccp.flying.youth.timeseriesdata.dao;

import com.casic.ccp.flying.youth.timeseriesdata.model.Data;
import com.casic.ccp.flying.youth.timeseriesdata.model.UploadFile;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈涛思数据库相关操作,显示库信息,创建表等〉
 *
 * @author: Sakura
 * @date: 2021/9/6
 */
@Mapper
public interface TaosDao {

    /**
     * 以超级表为模板,创建表
     * TAG 为  文件名称,表名,开始时间,结束时间
     *
     * @param tableName
     * @param uploadFile
     * @param timeMillis
     */
    @Insert("CREATE TABLE ${tableName} USING stb_time_data TAGS ('${uploadFile.name}','${uploadFile.anotherName}',${timeMillis},null)")
    void createTable(@Param("tableName") String tableName,@Param("uploadFile") UploadFile uploadFile, @Param("timeMillis") long timeMillis);

    /**
     * 更新表结束标签
     *
     * @param tableName
     * @param timeMillis
     */
    @Update("ALTER TABLE ${tableName} SET TAG end_time= #{timeMillis}")
    void updateTableEndTime(@Param("tableName") String tableName,@Param("timeMillis") long timeMillis);

    /**
     * 查询 通过不同的参数进行查询
     *
     * @param column  查询的列
     * @param tableName 表名
     * @param window 窗口
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    List<Data> select(@Param("column") String column, @Param("tableName") String tableName, @Param("window") Long window, @Param("startTime") Long startTime, @Param("endTime") Long endTime);

    /**
     * 查询一定时间区间内的 数据总数
     * @param tableName
     * @param startTime
     * @param endTime
     * @return
     */
    Long selectValueCount( @Param("tableName") String tableName, @Param("startTime") Long startTime, @Param("endTime") Long endTime);

    /**
     * 删除表
     * @param name
     */
    @Delete("DROP TABLE ${name};")
    void delTableByName(@Param("name") String name);
}

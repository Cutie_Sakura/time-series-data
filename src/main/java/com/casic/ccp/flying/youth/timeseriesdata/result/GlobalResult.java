package com.casic.ccp.flying.youth.timeseriesdata.result;

/**
 * vue列表需要的返回数据格式
 * @author: 飞航青年队
 */
public class GlobalResult {
    /**
     * 成功是20000，在request.js中可以自己规定各种前端需要状态码
     */
    private Integer code  = 20000;
    private ResultData data;

    public GlobalResult() {
    }

    public GlobalResult(ResultData data) {
        this.data = data;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public ResultData getData() {
        return data;
    }

    public void setData(ResultData data) {
        this.data = data;
    }
}

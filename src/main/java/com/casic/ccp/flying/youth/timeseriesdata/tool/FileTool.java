package com.casic.ccp.flying.youth.timeseriesdata.tool;

import java.util.UUID;

/**
 *  文件处理工具
 * @Author: 贺坤
 * @Date: 2021/9/9 19:37
 */

public class FileTool {
    /**
     * 生成唯一名称
     * @return
     */
   public static String generateUuidFileName(){
       return "data_"+UUID.randomUUID().toString().replaceAll("-","");
    }
}

package com.casic.ccp.flying.youth.timeseriesdata.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @Author: 贺坤
 * @Date: 2021/9/17 13:15
 */
@Configuration
@EnableAsync
public class TaskConfig {
    @Value("${time-series-data.task.corePoolSize}")
    private Integer corePoolSize = 12;
    @Value("${time-series-data.task.maxPoolSize}")
    private Integer maxPoolSize = 24;
    @Value("${time-series-data.task.queueCapacity}")
    private Integer queueCapacity = 10000;
    @Value("${time-series-data.task.keepAliveSeconds}")
    private Integer keepAliveSeconds = 300;
    @Value("${time-series-data.task.threadNamePrefix}")
    private String threadNamePrefix = "taskExecutor--";

    @Bean("taskThreadPoolTaskExecutor")
    public ThreadPoolTaskExecutor taskThreadPoolTaskExecutor(){
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        // 核心线程数
        taskExecutor.setCorePoolSize(corePoolSize);
        // 最大线程数
        taskExecutor.setMaxPoolSize(maxPoolSize);
        // 队列大小
        taskExecutor.setQueueCapacity(queueCapacity);
        // 持续时间
        taskExecutor.setKeepAliveSeconds(keepAliveSeconds);
        // 线程名称
        taskExecutor.setThreadNamePrefix(threadNamePrefix);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        //初始化
        taskExecutor.initialize();
        return taskExecutor;
    }

}

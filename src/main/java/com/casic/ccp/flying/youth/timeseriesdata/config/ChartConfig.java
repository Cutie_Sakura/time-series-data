package com.casic.ccp.flying.youth.timeseriesdata.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 图标配置配置
 * @Author: 贺坤
 * @Date: 2021/9/16 22:20
 */
@Configuration
public class ChartConfig {

    @Value("${time-series-data.chart.maxPoint}")
    private Long maxPoint = 1_0000L;
    /**
     * 最大显示点数
     * @return
     */
    @Bean
    public Long maxDisplayPoint(){
        return maxPoint;
    }

}

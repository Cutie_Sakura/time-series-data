/**
 * Copyright (C), 2021-2021, Sakura有限公司(尚未成立)
 * FileName: FileService
 * Author:   Sakura
 * Date:     2021/9/10 20:57
 * Description:
 * History: 修改历史
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.casic.ccp.flying.youth.timeseriesdata.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.casic.ccp.flying.youth.timeseriesdata.dao.UploadFileDao;
import com.casic.ccp.flying.youth.timeseriesdata.model.UploadFile;
import com.casic.ccp.flying.youth.timeseriesdata.tool.FileTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * 文件服务
 *
 * @author: Sakura
 * @date: 2021/9/10
 */
@Service
public class FileService extends ServiceImpl<UploadFileDao,UploadFile> {
    /**
     * 日志记录器
     */
    private static Logger LOGGER = LoggerFactory.getLogger(FileService.class);
    /**
     * 文档存储路径
     */
    @Autowired
    private File fileStorePath;
    /**
     * 定义文件后缀
     */
    public static final String SUFFIX = ".bin";

    /**
     * 返回一个存储数据文件的文件对象
     *
     * @return
     */
    public File preparingToStoreDataFile( String storeDataFileName) {

        File file = new File(fileStorePath, storeDataFileName+SUFFIX);
        return file;
    }

    /**
     * 生成存库记录
     * @param fileName 原来文件名
     * @return 磁盘中的唯一名称
     */
    public String save(String fileName) {
        // 得到存款唯一文件名
        String anotherName = FileTool.generateUuidFileName();
        File file = this.preparingToStoreDataFile(anotherName);
        UploadFile entity = new UploadFile();
        entity.setName(fileName);
        entity.setPath(fileStorePath.getPath());
        entity.setAnotherName(anotherName);
        entity.setFileSize(file.length());
        baseMapper.insert(entity);
        return anotherName;
    }
}

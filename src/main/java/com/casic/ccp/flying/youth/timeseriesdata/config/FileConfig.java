package com.casic.ccp.flying.youth.timeseriesdata.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.io.File;


/**
 * 文件存储配置
 * @Author: 贺坤
 * @Date: 2021/9/8 18:34
 */
@Configuration
public class FileConfig {
    /**
     * 日志记录器
     */
    private static Logger LOGGER = LoggerFactory.getLogger(FileConfig.class);
    @Value("${time-series-data.file.capacity}")
    private Integer capacity = 20000;
    @Value("${time-series-data.file.savePath}")
    private String savePath ;

    /**
     * 文件存储路径
     *
     * @return
     */
    @Bean
    public File fileStorePath() {
        ApplicationHome applicationHome = new ApplicationHome(getClass());
        File file = applicationHome.getSource();
        if (!StringUtils.isEmpty(savePath)){
            file = new File(savePath);
        }
        file = new File(file, "data");
        if (!file .exists()  && !file .isDirectory()) {
            boolean mkdir = file.mkdir();
            if (!mkdir) {
                String format = "{}创建失败!!!";
                LOGGER.error(format,file.getPath());}
        }
        return file;
    }

    /**
     * 设置文件读取字节数
     * @return
     */
    @Bean
    public Integer capacity(){
        return capacity;
    }

}

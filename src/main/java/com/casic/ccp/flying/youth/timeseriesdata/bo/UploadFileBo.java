package com.casic.ccp.flying.youth.timeseriesdata.bo;
/**
 * @author: 飞航青年队
 */
public class UploadFileBo extends BaseBo {
    private Long id;
    private String name;
    private String path;
    private String anotherName;

    @Override
    public String toString() {
        return "UploadFileBo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", anotherName='" + anotherName + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAnotherName() {
        return anotherName;
    }

    public void setAnotherName(String anotherName) {
        this.anotherName = anotherName;
    }
}

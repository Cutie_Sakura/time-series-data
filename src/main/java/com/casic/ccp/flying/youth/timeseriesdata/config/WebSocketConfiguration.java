package com.casic.ccp.flying.youth.timeseriesdata.config;

import com.casic.ccp.flying.youth.timeseriesdata.websocket.WebSocketMappingHandlerMapping;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;

/**
 * @Author: 贺坤
 * @Date: 2021/9/23 18:41
 */

@Configuration
public class WebSocketConfiguration {
    @Value("${time-series-data.webSocket.corePoolSize}")
    private Integer corePoolSize = 2;
    @Value("${time-series-data.webSocket.maxPoolSize}")
    private Integer maxPoolSize = 4;
    @Value("${time-series-data.webSocket.queueCapacity}")
    private Integer queueCapacity = 100;
    @Value("${time-series-data.webSocket.keepAliveSeconds}")
    private Integer keepAliveSeconds = 300;
    @Value("${time-series-data.webSocket.threadNamePrefix}")
    private String threadNamePrefix = "webSocketExecutor--";

    @Bean
    public HandlerMapping webSocketMapping() {
        return new WebSocketMappingHandlerMapping();
    }

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }

    @Bean("webSocketThreadPoolTaskExecutor")
    public ThreadPoolTaskExecutor taskThreadPoolTaskExecutorTwo() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        // 核心线程数
        taskExecutor.setCorePoolSize(corePoolSize);
        // 最大线程数
        taskExecutor.setMaxPoolSize(maxPoolSize);
        // 队列大小
        taskExecutor.setQueueCapacity(queueCapacity);
        // 持续时间
        taskExecutor.setKeepAliveSeconds(keepAliveSeconds);
        // 线程名称
        taskExecutor.setThreadNamePrefix(threadNamePrefix);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        //初始化
        taskExecutor.initialize();
        return taskExecutor;
    }
}

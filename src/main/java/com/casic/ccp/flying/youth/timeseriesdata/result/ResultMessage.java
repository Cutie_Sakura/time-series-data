package com.casic.ccp.flying.youth.timeseriesdata.result;
/**
 * @author: 飞航青年队
 */
public class ResultMessage {
    private Integer code  = 20000;
    private String title;
    private String message;
    private String type;

    public ResultMessage() {
    }

    public ResultMessage(String message, String type) {
        this.message = message;
        this.type = type;
    }

    public ResultMessage(Integer code, String message, String type) {
        this.code = code;
        this.message = message;
        this.type = type;
    }

    public ResultMessage(String title, String message, String type) {
        this.title = title;
        this.message = message;
        this.type = type;
    }

    public ResultMessage(Integer code, String title, String message, String type) {
        this.code = code;
        this.title = title;
        this.message = message;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

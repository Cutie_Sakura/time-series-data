package com.casic.ccp.flying.youth.timeseriesdata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.casic.ccp.flying.youth.timeseriesdata.model.UploadFile;
import org.apache.ibatis.annotations.Mapper;
/**
 * @author: 飞航青年队
 */
@Mapper
public interface UploadFileDao extends BaseMapper<UploadFile> {
}

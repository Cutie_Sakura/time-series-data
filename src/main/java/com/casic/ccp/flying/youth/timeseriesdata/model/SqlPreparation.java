/**
 * Copyright (C), 2021-2021, Sakura有限公司(尚未成立)
 * FileName: Pair
 * Author:   Sakura
 * Date:     2021/9/9 21:55
 * Description: 一对返回结果
 * History: 修改历史
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.casic.ccp.flying.youth.timeseriesdata.model;

/**
 * sql 准备
 *
 * @author: Sakura
 * @date: 2021/9/9
 */
public class SqlPreparation {

    /**
     * 字符串中的变量
     */
    private Object[] values;
    /**
     * 数据长度
     */
    private Integer length;

    /**
     * 时候可用
     */
    private boolean isActive;

    public SqlPreparation(Integer length) {
        this.values = new Object[length * 2];
        this.length = 0;
    }

    public String getSql(String tableName) {
        StringBuilder insertInto = new StringBuilder();
        insertInto.append("INSERT INTO ").append(tableName).append(" VALUES ");
        for (int i= 0 ; i< this.length;){
            insertInto.append("(").append(this.values[i++]).append(",").append(this.values[i++]).append(")");
        }

        return insertInto.toString();
    }

    /**
     * 顺序写入数据
     *
     * @param l
     * @param f
     * @return
     */
    public SqlPreparation sequentiallyWriteData(Long l, Float f) {
        this.values[this.length++] = l;
        this.values[this.length++] = f;
        return this;
    }

    /**
     * 清空数据
     */
    public void emptyValues() {
        this.length = 0;
//      this.values.clear();
    }

    public Object[] getValues() {
        return values;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public void destroy() {

    }
}

/**
 * Copyright (C), 2021-2021, Sakura有限公司(尚未成立)
 * FileName: PretreatmentSQLKey
 * Author:   Sakura
 * Date:     2021/9/10 22:01
 * Description: 预处理SQLKey的枚举
 * History: 修改历史
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.casic.ccp.flying.youth.timeseriesdata.model;

/**
 * 〈预处理SQLKey的枚举〉
 *  导入文件为1024*1024*1024 约10亿条
 *  10_0000_0000
 * @author: Sakura
 * @date: 2021/9/10
 */
public enum PretreatmentSQLKey {
    /**
     * 读取1000点的选项
     */
    Value_1K(1000),
    /**
     * 读取10000点的选项
     */
    Value_10K(10000),
    /**
     * 读取20000点的选项
     */
    Value_20K(20000),
    /**
     * 读取40000点的选项
     */
    Value_40K(40000),
    /**
     * 读取50000点的选项
     */
    Value_50K(50000),
    /**
     * 读取100000点的选项
     */
    Value_100K(100000),
    /**
     * 读取500000点的选项
     */
    Value_500K(500000),
    /**
     * 读取10000000点的选项
     */
    Value_1M(10000000),
    /**
     * 读取50000000点的选项
     */
    Value_5M(50000000);
    /**
     *私有熟悉
     */
    private Integer value;

    PretreatmentSQLKey(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    /**
     * 根据传入数量返回
     * @param value
     * @return
     */
    public static PretreatmentSQLKey getEnumByValue(Integer value){
        PretreatmentSQLKey[] values = PretreatmentSQLKey.values();
        for (int i = 0 ;i < values.length ; i++){
            if (values[i].getValue().equals(value)){
                return values[i];
            }
        }
        return null;
    }
}

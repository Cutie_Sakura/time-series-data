package com.casic.ccp.flying.youth.timeseriesdata.sql;

import com.casic.ccp.flying.youth.timeseriesdata.model.SqlPreparation;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.AbandonedConfig;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

/**
 * sql 准备类的对象池
 *
 * @Author: 贺坤
 * @Date: 2021/9/18 15:04
 */
public class SqlPreparationPool extends GenericObjectPool<SqlPreparation> {
    /**
     * 开关
     */
    private Boolean switchPool;
    /**
     * 对象工厂
     */
    private SqlPreparationFactory factory;

    public SqlPreparationPool(Boolean switchPool ,SqlPreparationFactory factory, GenericObjectPoolConfig<SqlPreparation> config) {
        super(factory, config);
        this.switchPool =switchPool;
        this.factory =factory;
    }

    private SqlPreparationPool(SqlPreparationFactory factory) {
        super(factory);
    }
    private SqlPreparationPool(SqlPreparationFactory factory, GenericObjectPoolConfig<SqlPreparation> config, AbandonedConfig abandonedConfig) {
        super(factory, config, abandonedConfig);
    }

    @Override
    public SqlPreparation borrowObject() throws Exception {
        if (switchPool){
            return super.borrowObject();
        }else {
            return factory.newSqlPreparation();
        }
    }
    @Override
    public void returnObject(SqlPreparation obj) {
        if (switchPool){
            super.returnObject(obj);
        }
    }
}

package com.casic.ccp.flying.youth.timeseriesdata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.casic.ccp.flying.youth.timeseriesdata.model.DataLoadInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: zhen_wei
 */
@Mapper
public interface DataLoadDao extends BaseMapper<DataLoadInfo> {
}

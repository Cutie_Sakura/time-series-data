package com.casic.ccp.flying.youth.timeseriesdata.bo;

import java.sql.Date;
/**
 * @author: 飞航青年队
 */
public class DataLoadInfoBo extends BaseBo{
    private Long id;
    private String fileTableId;
    private String name;
    private Date startTime;
    private Date endTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileTableId() {
        return fileTableId;
    }

    public void setFileTableId(String fileTableId) {
        this.fileTableId = fileTableId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

}

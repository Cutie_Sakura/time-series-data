package com.casic.ccp.flying.youth.timeseriesdata.bo;

import com.casic.ccp.flying.youth.timeseriesdata.model.AlgorithmType;

/**
 * 折线图查询对象
 *
 * @Author: 贺坤
 * @Date: 2021/9/15 11:06
 */
public class ChartBo {
    /**
     * id
     */
    private Long id;
    /**
     * 表名
     */
    private String name;
    /**
     * 开始时间
     */
    private Long startTime;
    /**
     * 结束时间
     */
    private Long endTime;
    /**
     * 算法类型
     */
    private AlgorithmType type;

    /**
     * 窗口
     */
    private Long window;
    /**
     * 窗口系数
     */
    private Integer windowRatio = 1;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public AlgorithmType getType() {
        return type;
    }

    public void setType(AlgorithmType type) {
        this.type = type;
    }

    public Long getWindow() {
        return window;
    }

    public void setWindow(Long window) {
        this.window = window;
    }

    public Integer getWindowRatio() {
        return windowRatio;
    }

    public void setWindowRatio(Integer windowRatio) {
        this.windowRatio = windowRatio;
    }
}

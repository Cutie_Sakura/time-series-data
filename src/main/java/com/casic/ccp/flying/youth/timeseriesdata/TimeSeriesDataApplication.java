package com.casic.ccp.flying.youth.timeseriesdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
/**
 * @author: 飞航青年队
 */
@SpringBootApplication
@EnableCaching
public class TimeSeriesDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimeSeriesDataApplication.class, args);
    }

}

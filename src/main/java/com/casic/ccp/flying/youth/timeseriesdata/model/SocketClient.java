/**
 * Copyright (C), 2021-2021, Sakura有限公司(尚未成立)
 * FileName: SocketClient
 * Author:   Sakura
 * Date:     2021/9/23 21:23
 * Description:
 * History: 修改历史
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.casic.ccp.flying.youth.timeseriesdata.model;

import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.FluxSink;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author: Sakura
 * @date: 2021/9/23
 */
public class SocketClient {
    private FluxSink<WebSocketMessage> sink;
    private WebSocketSession session;

    public SocketClient(FluxSink<WebSocketMessage> sink, WebSocketSession session) {
        this.sink = sink;
        this.session = session;
    }

    public void sendData(String data) {
        if (sink == null || session == null) {return;}
        sink.next(session.textMessage(data));
    }

    public FluxSink<WebSocketMessage> getSink() {
        return sink;
    }

    public void setSink(FluxSink<WebSocketMessage> sink) {
        this.sink = sink;
    }

    public WebSocketSession getSession() {
        return session;
    }

    public void setSession(WebSocketSession session) {
        this.session = session;
    }
}

package com.casic.ccp.flying.youth.timeseriesdata.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.casic.ccp.flying.youth.timeseriesdata.bo.DataLoadInfoBo;
import com.casic.ccp.flying.youth.timeseriesdata.dao.DataLoadDao;
import com.casic.ccp.flying.youth.timeseriesdata.model.DataLoadInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author: 飞航青年队
 */
@Service
public class DataLoadService extends ServiceImpl<DataLoadDao, DataLoadInfo> {
    /**
     * 日志记录器
     */
    private static Logger LOGGER = LoggerFactory.getLogger(DataLoadService.class);

    @Autowired
    private DataLoadDao dataLoadDao;

    /**
     * 获取数据列表
     *
     * @return
     */
    public IPage<DataLoadInfo> getList(DataLoadInfoBo dataLoadInfoBo) {
        IPage<DataLoadInfo> dataPage = new Page<>(dataLoadInfoBo.getPage(), dataLoadInfoBo.getLimit());
        QueryWrapper<DataLoadInfo> wrapper = new QueryWrapper<>();
        wrapper.like(!StringUtils.isEmpty(dataLoadInfoBo.getName()), "name", dataLoadInfoBo.getName());
        IPage<DataLoadInfo> pages = dataLoadDao.selectPage(dataPage, wrapper);
        return pages;
    }

    /**
     * 删除一个数据通过ID
     */
    public void delete(Long id) {
        dataLoadDao.deleteById(id);
    }
}

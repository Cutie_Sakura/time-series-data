package com.casic.ccp.flying.youth.timeseriesdata.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * 〈一句话功能简述〉<br>
 * 文件加载信息
 *
 * @author: 伟
 * @date: 2021/9/11
 */
@TableName(value = "Data_Tables")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataLoadInfo {
    /**
     * id
     */
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 文件表id
     */
    @TableField(value = "File_Table_id")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long fileTableId;
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 名称
     */
    private String name;
    /**
     * 开始时间
     */
    @TableField(value = "start_time")
    private Long startTime;
    /**
     * 结束时间
     */
    @TableField(value = "end_time")
    private Long endTime;

    public DataLoadInfo() {

    }
    public DataLoadInfo(Long id, Long fileTableId, String name, Long startTime, Long endTime) {
        this.id = id;
        fileTableId = fileTableId;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFileTableId() {
        return fileTableId;
    }

    public void setFileTableId(Long fileTableId) {
        this.fileTableId = fileTableId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}

/**
 * Copyright (C), 2021-2021, Sakura有限公司(尚未成立)
 * FileName: TaosService
 * Author:   Sakura
 * Date:     2021/9/6 20:27
 * Description: 涛思数据的service层
 * History: 修改历史
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.casic.ccp.flying.youth.timeseriesdata.service;

import com.casic.ccp.flying.youth.timeseriesdata.model.DataLoadInfo;
import com.casic.ccp.flying.youth.timeseriesdata.model.SocketClient;
import com.casic.ccp.flying.youth.timeseriesdata.model.UploadFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 〈一句话功能简述〉<br>
 * 〈涛思数据的service层〉
 *
 * @author: Sakura
 * @date: 2021/9/6
 */
@Service
public class WebSocketService {
    /**
     * 文件管理
     */
    @Autowired
    FileService fileService;
    /**
     * 涛思数据 服务
     */
    @Autowired
    TaosService taosService;

    @Autowired
    private DataLoadService dataLoadService;
    /**
     * 日志记录器
     */
    private static Logger LOGGER = LoggerFactory.getLogger(WebSocketService.class);


    @Async("webSocketThreadPoolTaskExecutor")
    public void handle(String msg, SocketClient socketClient) {
        long startTime = System.currentTimeMillis();
        UploadFile uploadFile = fileService.getById(Long.valueOf(msg));
        DataLoadInfo dataLoadInfo = taosService.batchDataWrit(uploadFile, socketClient);
        dataLoadService.save(dataLoadInfo);
        long endTime = System.currentTimeMillis();
        LOGGER.info("文件:{},导入耗时{}毫秒", dataLoadInfo.getFileName(), (endTime - startTime));
    }

}

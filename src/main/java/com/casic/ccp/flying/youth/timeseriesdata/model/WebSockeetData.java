/**
 * Copyright (C), 2021-2021, Sakura有限公司(尚未成立)
 * FileName: WebSockeetData
 * Author:   Sakura
 * Date:     2021/9/23 20:37
 * Description: WebSocket数据模型
 * History: 修改历史
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.casic.ccp.flying.youth.timeseriesdata.model;

/**
 * 〈一句话功能简述〉<br>
 * 〈WebSocket数据模型〉
 *
 * @author: Sakura
 * @date: 2021/9/23
 */
public class WebSockeetData {
    /**
     * 状态码
     *  0   --  文件总大小
     *  1   --  本次读取到的大小
     *  2   --  数据库写入成功
     *  3   --  处理完成
     */
    private Integer state;
    private Long value;

    public WebSockeetData(Integer state, Long value) {
        this.state = state;
        this.value = value;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"state\":\"" + state + "\" , " +
                "\"value\":\"" + value + "\" }"
                ;
    }
}

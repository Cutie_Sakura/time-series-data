package com.casic.ccp.flying.youth.timeseriesdata.sql;

import com.casic.ccp.flying.youth.timeseriesdata.model.SqlPreparation;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 对象工厂
 * @Author: 贺坤
 * @Date: 2021/9/18 14:56
 */
@Component
public class SqlPreparationFactory implements PooledObjectFactory<SqlPreparation> {
    @Autowired
    Integer capacity;

    /**
     * 得到一个对象
     * @return
     */
    public SqlPreparation newSqlPreparation() {
        return new SqlPreparation(capacity);
    }

    /**
     * /构造一个封装对象
     *
     * @return
     */
    @Override
    public PooledObject<SqlPreparation> makeObject() {
        return new DefaultPooledObject<>(newSqlPreparation());
    }
    /**
     * 销毁对象
     *
     * @param p
     * @throws Exception
     */
    @Override
    public void destroyObject(PooledObject<SqlPreparation> p){
        p.getObject().destroy();
    }
    /**
     * 验证对象是否可用
     *
     * @param p
     * @return
     */
    @Override
    public boolean validateObject(PooledObject<SqlPreparation> p) {
        return p.getObject().isActive();
    }
    /**
     * 激活一个对象，使其可用用
     *
     * @param p
     * @throws Exception
     */
    @Override
    public void activateObject(PooledObject<SqlPreparation> p) throws Exception {
        p.getObject().setActive(true);
    }
    /**
     * 钝化一个对象,也可以理解为反初始化
     *
     * @param p
     * @throws Exception
     */
    @Override
    public void passivateObject(PooledObject<SqlPreparation> p) throws Exception {

    }
}

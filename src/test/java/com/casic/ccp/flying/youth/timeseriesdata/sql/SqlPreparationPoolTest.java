package com.casic.ccp.flying.youth.timeseriesdata.sql;

import com.casic.ccp.flying.youth.timeseriesdata.controller.ChartController;
import com.casic.ccp.flying.youth.timeseriesdata.model.SqlPreparation;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SqlPreparationPoolTest {
    /**
     * 日志记录器
     */
    private static Logger LOGGER = LoggerFactory.getLogger(SqlPreparationPoolTest.class);
    /**
     * 对象池
     */
    @Autowired
    private SqlPreparationPool sqlPreparationPool;
    @Test
    public void test() throws Exception {
        long start0 = System.nanoTime();
        SqlPreparation sqlPreparation = sqlPreparationPool.borrowObject();
        long end0 = System.nanoTime();
        LOGGER.info("从池中获取对象需要耗时{}",end0-start0);

        long start1 =  System.nanoTime();
        sqlPreparationPool.returnObject(sqlPreparation);
        long end1 =  System.nanoTime();
        LOGGER.info("从池中还对象需要耗时{}",end1-start1);

        long start2 =  System.nanoTime();
        SqlPreparation sqlPreparation1 = new SqlPreparation(20000);
        long end2 =  System.nanoTime();
        LOGGER.info("new一个对象需要耗时{}",end2-start2);
    }

}
package com.casic.ccp.flying.youth.timeseriesdata.service;

import com.casic.ccp.flying.youth.timeseriesdata.model.SqlPreparation;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class JdbcServiceTest {
    /**
     * 日志记录器
     */
    private static Logger LOGGER = LoggerFactory.getLogger(JdbcServiceTest.class);

    @Autowired
   private JdbcService jdbcService;

    @Test
    public void test() throws Exception {
        long start0 = System.nanoTime();
        SqlPreparation sqlPreparation = new SqlPreparation(20000);
        long timeMillis = System.currentTimeMillis();
        for (int i = 0 ; i < 20000 ; i++){
            sqlPreparation.sequentiallyWriteData(timeMillis++,Double.valueOf(Math.random()).floatValue());
        }
        jdbcService.insertBatchData(sqlPreparation, "data_test");
        long end0 = System.nanoTime();
        LOGGER.info("插入数据耗时{}",end0-start0);
        LOGGER.info("-------------------------------");
        LOGGER.info("第{}次,{}",1,"插入数据耗时113914700");
        LOGGER.info("第{}次,{}",2,"插入数据耗时121593000");
        LOGGER.info("第{}次,{}",3,"插入数据耗时120207000");
        LOGGER.info("-------------------------------");
        LOGGER.info("第{}次,{}",1,"插入数据耗时267537368700 <<< 错误 ");
        LOGGER.info("第{}次,{}",2,"");
        LOGGER.info("第{}次,{}",3,"");
    }
}
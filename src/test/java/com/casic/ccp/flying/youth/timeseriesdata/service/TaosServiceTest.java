package com.casic.ccp.flying.youth.timeseriesdata.service;


import com.casic.ccp.flying.youth.timeseriesdata.model.SocketClient;
import com.casic.ccp.flying.youth.timeseriesdata.model.UploadFile;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

import static com.casic.ccp.flying.youth.timeseriesdata.tool.FileTool.GenerateUUIDFileName;


@SpringBootTest
class TaosServiceTest {
    /**
     * 日志记录器
     */
    private static Logger LOGGER = LoggerFactory.getLogger(TaosServiceTest.class);


    @Autowired
    TaosService taosService;
    /**
     * 文档存储路径
     */
    @Autowired
    private File fileStorePath;

    int capacity = 100;

    /**
     *  测试结果,插入1K点位的数据 需要300毫秒
     *  推测1G 点位
     */
    @Test
    void batchDataWrit() {

        File file = new File("C:\\sakura\\time-series-data\\data\\demo.bin");
        UploadFile uploadFile = new UploadFile();
        uploadFile.setId(0L);
        uploadFile.setName("demo");
        uploadFile.setPath(fileStorePath.getPath());
        uploadFile.setAnotherName(GenerateUUIDFileName());
        long time0 = System.currentTimeMillis();
        taosService.batchDataWrit(uploadFile,new SocketClient(null,null));
        long time1 = System.currentTimeMillis();
        LOGGER.info(uploadFile.getAnotherName());
        LOGGER.info("开始于{},结束于{}, 差值{}",time0,time1,time1-time0);
    }

}
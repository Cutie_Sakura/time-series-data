package com.casic.ccp.flying.youth.timeseriesdata;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.casic.ccp.flying.youth.timeseriesdata.dao.UploadFileDao;
import com.casic.ccp.flying.youth.timeseriesdata.model.UploadFile;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class TimeSeriesDataApplicationTests {

    @Resource
    private UploadFileDao uploadFileDao;

    @Test
    void contextLoads() {
        List<UploadFile> list = uploadFileDao.selectList(null);
        for (UploadFile uploadFile : list) {
            System.out.println(uploadFile);
        }
    }

    @Test
    void add(){
        UploadFile uploadFile = new UploadFile();
        uploadFile.setName("ajhaj");
        uploadFile.setPath("ajhaj");
        uploadFile.setAnotherName("ajhaj");
        int insert = uploadFileDao.insert(uploadFile);
        System.out.println(insert);
    }

    @Test
    public void testFinndPage(){
        IPage<UploadFile> page = new Page<>(1,2);
        IPage<UploadFile> uploadFileIPage = uploadFileDao.selectPage(page, null);
        List<UploadFile> records = uploadFileIPage.getRecords();
        for (UploadFile record : records) {
            System.out.println(record);
        }
    }

}

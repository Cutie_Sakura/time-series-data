/**
 * Copyright (C), 2021-2021, Sakura有限公司(尚未成立)
 * FileName: FileNioTest
 * Author:   Sakura
 * Date:     2021/10/17 21:39
 * Description:
 * History: 修改历史
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.casic.ccp.flying.youth.timeseriesdata;

import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author: Sakura
 * @date: 2021/10/17
 */
public class FileNioTest {
    /**
     * 制作一个缩小10倍的文件
     * @throws IOException
     */
    @Test
    public void test0() throws IOException {
        FileInputStream in = new FileInputStream("C:\\sakura\\time-series-data\\data\\demo1G.bin");
        FileOutputStream out = new FileOutputStream("C:\\sakura\\time-series-data\\data\\demo100M.bin");
        FileChannel inch = in.getChannel();
        FileChannel outch = out.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(4_0000_0);
        for ( int i = 0 ; i<1000 ; i++){
            inch.read(byteBuffer);
            //切换读取模式
            byteBuffer.flip();
            outch.write(byteBuffer);
            byteBuffer.clear();
        }
        inch.close();
        outch.close();
        in.close();
        out.close();
    }

    /**
     * 将文件中写入一个极大的值和一个极小的值
     * @throws IOException
     */
    @Test
    public void test1() throws IOException {
        FileInputStream in = new FileInputStream("C:\\sakura\\time-series-data\\data\\demo1G.bin");
        FileOutputStream out = new FileOutputStream("C:\\sakura\\time-series-data\\data\\demo100M+2.bin");
        FileChannel inch = in.getChannel();
        FileChannel outch = out.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(4_0000_0);
        for ( int i = 0 ; i<500 ; i++){
            inch.read(byteBuffer);
            //切换读取模式
            byteBuffer.flip();
            outch.write(byteBuffer);
            byteBuffer.clear();
        }
        ByteBuffer byteBuffer0 = ByteBuffer.allocate(8);
        byteBuffer0.order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer0.putFloat(Float.valueOf(-475.83334F));
        byteBuffer0.putFloat(Float.valueOf(4758.3334F));
        byteBuffer0.flip();
        outch.write(byteBuffer0);
        for ( int i = 0 ; i<500 ; i++){
            inch.read(byteBuffer);
            //切换读取模式
            byteBuffer.flip();
            outch.write(byteBuffer);
            byteBuffer.clear();
        }
        inch.close();
        outch.close();
        in.close();
        out.close();
    }

    /**
     * 查看中间位置 的数值是多少
     * @throws IOException
     */
    @Test
    public void test2() throws IOException {
        FileInputStream in = new FileInputStream("C:\\sakura\\time-series-data\\data\\demo1G.bin");

        FileChannel inch = in.getChannel();

        ByteBuffer byteBuffer = ByteBuffer.allocate(4_0000_0);
        for ( int i = 0 ; i<500 ; i++){
            inch.read(byteBuffer);
            //切换读取模式
            byteBuffer.flip();
            byteBuffer.clear();
        }
        inch.read(byteBuffer);
        byteBuffer.flip();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        System.out.println("数值为:" + byteBuffer.getFloat());

        inch.close();
        in.close();
    }
}

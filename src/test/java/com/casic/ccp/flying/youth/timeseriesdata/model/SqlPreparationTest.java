package com.casic.ccp.flying.youth.timeseriesdata.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author: 贺坤
 * @Date: 2021/9/18 15:43
 */
class SqlPreparationTest {
    @Test
    public void test0(){
        SqlPreparation sqlPreparation = new SqlPreparation(10);
        for (Integer i = 0; i < 10 ; i++){
            sqlPreparation.sequentiallyWriteData(i.longValue(),i.floatValue());
        }
        System.out.println(sqlPreparation.getSql("test0"));
        sqlPreparation.emptyValues();
        for (Integer i = 0; i < 5 ; i++){
            sqlPreparation.sequentiallyWriteData(i.longValue()*2, (float) (i.floatValue()*Math.PI));
        }
        System.out.println(sqlPreparation.getSql("test1"));
    }

}
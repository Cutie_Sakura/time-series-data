import request from '@/utils/request'

// 请求折线图数据
export function chartList(query) {
  return request({
    url: '/chart',
    method: 'get',
    params: query
  })
}

/**
 * 获取表的列表
 * @returns {AxiosPromise}
 */
export function tableList() {
  return request({
    url: '/chart/table',
    method: 'get'
  })
}


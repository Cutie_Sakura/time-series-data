import request from '@/utils/request'

// 文件列表数据的请求地址
export function fetchList(query) {
  // 会在request中发送具体的请求
  return request({
    url: '/file/getFileList',
    method: 'get',
    params: query
  })
}

export function importData(data) {
  return request({
    url: '/file/importData',
    method: 'post',
    params: data
  })
}

export function deleteFile(data) {
  return request({
    url: '/file/deleteFile',
    method: 'post',
    params: data
  })
}

export function downloadFile(data) {
  return request({
    url: '/file/downloadFile',
    method: 'post',
    params: data
  })
}

export function fetchArticle(query) {
  return request({
    url: '/data/getDataList',
    method: 'get',
    params: query
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function deleteDate(data) {
  return request({
    url: '/data/delete',
    method: 'post',
    params: data
  })
}

export function updateArticle(data) {
  return request({
    url: '/vue-element-admin/article/update',
    method: 'post',
    data
  })
}

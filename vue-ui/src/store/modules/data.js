const getDefaultState = () => {
  return {
    dataLoadInfo: null
  }
}

const state = getDefaultState()
const mutations = {
  SET_DATA_LOAD_INFO: (state, dataLoadInfo) => {
    state.dataLoadInfo = dataLoadInfo
  }
}
const actions = {
  setDataLoadInfo({ commit }, dataLoadInfo) {
    commit('SET_DATA_LOAD_INFO', dataLoadInfo)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

